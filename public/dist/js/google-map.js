// hack Google Maps to bypass API v3 key (needed since 22 June 2016 http://googlegeodevelopers.blogspot.com.es/2016/06/building-for-scale-updates-to-google.html)
var target = document.head;
var observer = new MutationObserver(function(mutations) {
    for (var i = 0; mutations[i]; ++i) { // notify when script to hack is added in HTML head
        if (mutations[i].addedNodes[0].nodeName == "SCRIPT" && mutations[i].addedNodes[0].src.match(/\/AuthenticationService.Authenticate?/g)) {
            var str = mutations[i].addedNodes[0].src.match(/[?&]callback=.*[&$]/g);
            if (str) {
                if (str[0][str[0].length - 1] == '&') {
                    str = str[0].substring(10, str[0].length - 1);
                } else {
                    str = str[0].substring(10);
                }
                var split = str.split(".");
                var object = split[0];
                var method = split[1];
                window[object][method] = null; // remove censorship message function _xdc_._jmzdv6 (AJAX callback name "_jmzdv6" differs depending on URL)
                //window[object] = {}; // when we removed the complete object _xdc_, Google Maps tiles did not load when we moved the map with the mouse (no problem with OpenStreetMap)
            }
            observer.disconnect();
        }
    }
});
var config = { attributes: true, childList: true, characterData: true }
observer.observe(target, config);

function init() {

    var myLatlng 	= new google.maps.LatLng(51.5118416,-0.1476274);
    var styles 		= [ {elementType:"geometry",stylers:[{"color":"#f5f5f5"}]},{elementType:"labels.icon",stylers:[{"visibility":"off"}]},{elementType:"labels.text.fill",stylers:[{"color":"#616161"}]},{elementType:"labels.text.stroke",stylers:[{"color":"#f5f5f5"}]},{featureType:"administrative.land_parcel",elementType:"labels.text.fill",stylers:[{"color":"#bdbdbd"}]},{featureType:"poi",elementType:"geometry",stylers:[{"color":"#eeeeee"}]},{featureType:"poi",elementType:"labels.text.fill",stylers:[{"color":"#757575"}]},{featureType:"poi.park",elementType:"geometry",stylers:[{"color":"#e5e5e5"}]},{featureType:"poi.park",elementType:"labels.text.fill",stylers:[{"color":"#9e9e9e"}]},{featureType:"road",elementType:"geometry",stylers:[{"color":"#ffffff"}]},{featureType:"road.arterial",elementType:"labels",stylers:[{"visibility":"off"}]},{featureType:"road.arterial",elementType:"labels.text.fill",stylers:[{"color":"#757575"}]},{featureType:"road.highway",elementType:"geometry",stylers:[{"color":"#dadada"}]},{featureType:"road.highway",elementType:"labels",stylers:[{"visibility":"off"}]},{featureType:"road.highway",elementType:"labels.text.fill",stylers:[{"color":"#616161"}]},{featureType:"road.local",stylers:[{"visibility":"off"}]},{featureType:"road.local",elementType:"labels.text.fill",stylers:[{"color":"#9e9e9e"}]},{featureType:"transit.line",elementType:"geometry",stylers:[{"color":"#e5e5e5"}]},{featureType:"transit.station",elementType:"geometry",stylers:[{"color":"#eeeeee"}]},{featureType:"water",elementType:"geometry",stylers:[{"color":"#c9c9c9"}]},{featureType:"water",elementType:"labels.text.fill",stylers:[{"color":"#9e9e9e"}]} ];
    var styledMap 	= new google.maps.StyledMapType(styles,{name: "Styled Map"});
    var mapOptions 	= {
        draggable: true,
        zoomControl: false,
        disableDoubleClickZoom: true,
        zoom: 12,
        center: myLatlng,
        disableDefaultUI: true,
        scrollwheel: !1,
        mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
        }
    };

    var selector        = document.getElementById('google-map-1');
    var map 		    = new google.maps.Map(selector,mapOptions);
    var image 		    = 'dist/images/map-pin.png';
    var contentString   = '<div class="map-pin-content">'+ selector.dataset.pin +'</div>';
    var infowindow      = new google.maps.InfoWindow({
        content: contentString
    });

    map.mapTypes.set('map_style', styledMap);
    map.setMapTypeId('map_style');

    var beachMarker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        icon: image
    });

    //infowindow.open(map,beachMarker); // If you want to show the Address pin by Default

    google.maps.event.addListener(beachMarker, 'click', function() {
        infowindow.open(map,beachMarker);
    });

    google.maps.event.addDomListener(window, "resize", function() {
        var center = map.getCenter();
        google.maps.event.trigger(map, "resize");
        map.setCenter(center);
    });
}
google.maps.event.addDomListener(window, "load", init);