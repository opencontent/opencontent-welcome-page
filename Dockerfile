FROM caddy:2.5.1

ADD public /srv

EXPOSE 80

CMD [ "caddy", "file-server", "--listen", "0.0.0.0:80" ]

